import {
  Controller,
  Get,
  Post,
  Body,
  Query,
  Put,
  Logger,
  UsePipes,
  ValidationPipe,
  Param,
} from '@nestjs/common';
import {
  ClientProxy,
  ClientProxyFactory,
  Transport,
} from '@nestjs/microservices';
import { Observable } from 'rxjs';
import { AtualizarCategoriaDto } from './dtos/atualizar-categoria.dto';
import { CriarCategoriaDto } from './dtos/criar-categoria.dto';

@Controller('api/v1')
export class AppController {
  private logger = new Logger(AppController.name);

  private clienteAdminBackend: ClientProxy;

  //Se registrando no RabbitMQ - no virtual host smartranking e na fila ADMIN-BACKEND.
  constructor() {
    this.clienteAdminBackend = ClientProxyFactory.create({
      transport: Transport.RMQ,
      options: {
        urls: ['amqp://user:bitnami@localhost:5672/smartranking'],
        queue: 'admin-backend',
      },
    });
  }

  //Inserindo novas categorias
  @Post('categorias')
  @UsePipes(ValidationPipe)
  criarCategoria(@Body() criarCategoriaDto: CriarCategoriaDto) {
    this.clienteAdminBackend.emit('criar-categoria', criarCategoriaDto);
  }

  //consultar categoria pelo _id ou todas as categorias
  @Get('categorias')
  consultarCategoria(@Query('idCategoria') _id: string): Observable<any> {
    return this.clienteAdminBackend.send(
      'consultar-categorias',
      _id ? _id : '',
    );
  }

  //atualizando novas categorias
  @Put('categorias/:_id')
  @UsePipes(ValidationPipe)
  atualizarCategoria(
    @Body() atualizarCategoriaDto: AtualizarCategoriaDto,
    @Param('_id') _id: string,
  ) {
    this.clienteAdminBackend.emit('atualizar-categoria', {
      id: _id,
      categoria: AtualizarCategoriaDto,
    });
  }
}
